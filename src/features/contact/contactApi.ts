import http, {skipToken} from '../httpManage';

import { IContactData } from '../types/Contact';

export const contactAPI = http.injectEndpoints({
  endpoints: (build) => ({
    getContacts: build.query<Array<IContactData>, void>({
      query: () => ({
        url: '/contacts',
        params: {
          '$count': 'true',
          '$top': 1000,
          '$select': 'e4a',
        },
      }),
      providesTags: ['Contact'],
    })
  }),
  overrideExisting: false,
})

export const {
  useGetContactsQuery,
} = contactAPI;

export {
  skipToken,
}

export default contactAPI;
