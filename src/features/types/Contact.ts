
export interface IContactData{
    id: string,
    title: string
    displayName: string,
}

export type ContactData = IContactData;
