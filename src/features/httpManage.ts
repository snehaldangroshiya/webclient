import { createApi, fetchBaseQuery, skipToken } from '@reduxjs/toolkit/query/react';

export default createApi({
  baseQuery: fetchBaseQuery({
    baseUrl: '/api/v1',
  }),
  tagTypes: [
    'Contact',
  ],
  refetchOnMountOrArgChange: 30,
  endpoints: () => ({}),
});


export {
  skipToken,
}
